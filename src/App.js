import ClassCounter from "./components/ClassCounter";
import HookCounter from "./components/HookCounter";
import UpdownCounter from "./components/UpdownCounter";


function App() {
  return (
    <div>
      {/* <ClassCounter/> */}
      <HookCounter/>
      <UpdownCounter/>
    </div>
  );
}

export default App;
