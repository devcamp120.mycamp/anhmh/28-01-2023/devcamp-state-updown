import React, {useState} from "react";

function UpdownCounter() {
    const initialCount = 0
    const [count, setCount] = useState(initialCount)
    return(
        <div style={{margin: "50px"}}>
            Count: {count} 
            <button onClick={() => setCount(initialCount)} style={{margin: "10px", background: "orange", color: "black"}}>Reset</button>
            <button onClick={() => setCount(count + 1)} style={{margin: "5px", background: "green", color: "white"}}>Increment</button>
            <button onClick={() => setCount(count - 1)} style={{margin: "5px", background: "red", color: "white"}}>Decrement</button>
        </div>
    )
}
export default UpdownCounter